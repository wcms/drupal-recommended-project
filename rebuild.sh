#!/bin/bash

# Global variable for the files we are using.
FILE=composer.lock

# Global variable for the type of environment.
env_option=0
build_option=0
os_option=0

# Parse out the default Ohana version from the composer.json file.
default_ohana_version=`grep "wcms/uw_wcms_ohana" composer.json | cut -f4 -d "\""`
# Remove "dev-" if it exists
default_ohana_version=${default_ohana_version/dev-/}

# Global variables for types of installs.
defaultinstall="Rebuild profile and ohana (for theme development)"
noohanainstall="Rebuild profile only (for non-theme development)"
onlyohanainstall="Rebuild ohana only (for theme development)"
updatecomposerinstall="Rebuild composer tags"

# Include the scripts that have required functions.
. "scripts/composeroption.sh"
. "scripts/getoptionsuser.sh"
. "scripts/setenvoption.sh"

# Main program.
if [ -f "composer.json" ]; then

  # If there are command line arguments, get
  # the options from command line, if not then
  # get the options from user input.
  if [ ! $# -eq 0 ]; then
    # Check the last argument for our "git switch".
    if [ ${*: -1} == '--no-retry-config' ]; then
      no_retry_config=1
      # Remove this from the list of arguments to avoid confusing the rest of the script.
      # From https://stackoverflow.com/questions/20398499/remove-last-argument-from-argument-list-of-shell-script-bash
      set -- "${@:1:$(($#-1))}"
    else
      no_retry_config=0
    fi
  fi

  if [ ! $# -eq 0 ]; then
    if ! [[ "$1" =~ ^[0-9]+$ ]] || [ "$1" -lt "1" ] || [ "$1" -gt "4" ]; then
      echo 'Error: Invalid command line option for environment.'
      exit 1;
    fi

    if ! [[ "$2" =~ ^[0-9]+$ ]] || [ "$2" -lt "1" ] || [ "$2" -gt "2" ]; then
      echo 'Error: Invalid command line option for operating system.'
      exit 1;
    fi

    if [ "$3" != "y"  ] && [ "$3" != "n"  ]; then
      echo 'Error: Invalid command line option for WCMS config.'
      exit 1;
    fi

    if [[ $1 == "" ]]; then
      env_option=2
    else
      env_option="$1"
    fi

    setenvoption

    if [[ $2 == "" ]]; then
      build_option=1
    else
      build_option="$2"
    fi

    if [[ $3 == "" ]]; then
      os_option=1
    else
      os_option="$3"
    fi

    if [[ $4 == "" ]]; then
      ohana_version="$default_ohana_version"
    else
      ohana_version="$4"

      existed_in_remote=$(git ls-remote --quiet --heads --tags https://git.uwaterloo.ca/wcms/uw_wcms_ohana.git ${ohana_version} 2> /dev/null)

      if [[ -z ${existed_in_remote} ]]; then
        echo "Error: invalid Ohana branch/tag."
        exit 1;
      fi
    fi

    if [[ $5 == "" ]]; then
      fonts_option='y'
    else
      fonts_option="$5"
    fi

    # If the choice is Pantheon (option 4), then
    # we have to force to only rebuild profile.
    if [[ $env_option == 4 ]]; then
      build_option=2
    fi

  else
    getoptionsuser
  fi

#  echo "ENV: $env_option"
#  echo "BUILD: $build_option"
#  echo "OS: $os_option"
#  echo "Ohana: $ohana_version"
#  echo "PL: $PL_PATH"
#  echo "DRUPAL: $DRUPAL_PATH"
#  echo "WCMS Config: $wcms_option"

  composeroption
fi
