function setenvoption {

  case $env_option in
    1) PL_PATH="/var/www/html"
      script_path=`pwd`
      if [[ "$script_path" == *"alt"* ]]; then
        DRUPAL_PATH=/var/www/drupal8alt
      else
        DRUPAL_PATH=/var/www/drupal8
      fi;;
    2) PL_PATH="/var/www/html/web"
       DRUPAL_PATH="/var/www/html";;
    3) PL_PATH="/app/wcms-artifact/web"
       DRUPAL_PATH="/app/wcms-artifact";;
    4) PL_PATH=`pwd`
       DRUPAL_PATH=`pwd`;;
  esac
}
