function composeroption {

  if [[ $wcms_option == 'y' ]]; then
    echo "**************************************************************************************"
    echo "Cloning wcms config ..."
    if [[ -d config ]]; then
      rm -rf config
    fi
    while ! git clone --branch wcms-3 https://git.uwaterloo.ca/wcms/config.git; do
      if [[ $no_retry_config == 1 ]]; then
        retry_option=n
      else
        echo ""
        read -rp "Unable to clone. Retry? [Y/n/abort]: " retry_option
      fi

      if [[ -z ${retry_option} ]]; then
        retry_option=Y
      fi

      if [[ ! ${retry_option} =~ ^(y|Y|n|N|abort)$ ]]; then
        echo "Invalid choice ..."
      fi

      if [[ ${retry_option} =~ ^(n|N)$ ]]; then
        echo "*** CONTINUING WITHOUT CLONING WCMS CONFIG ***"
        break
      fi

      if [[ $retry_option == "abort" ]]; then
        echo "Aborting rebuild script."
        exit 1
      fi
    done
    echo "Done cloning wcms config."
    echo "**************************************************************************************"
  fi

  if [[ $build_option == 1 || $build_option == 2 ]]; then

    if [ -f "$FILE" ]; then
      echo "Composer.lock exists, removing composer.lock ..."
      rm composer.lock
      echo "Done removing composer.lock."
    fi

    echo "**************************************************************************************"
    echo "Removing composer.lock, profile/uw_base_profile, ohana symlink and vendor ..."
    rm -rf web/profiles/uw_base_profile
    rm -rf vendor/
    if [[ -L "$PL_PATH/uw_wcms_ohana" || -d "$PL_PATH/uw_wcms_ohana" ]]; then
      rm -rf $PL_PATH/uw_wcms_ohana
    fi
    echo "Done removing composer.lock, profile/uw_base_profile and vendor."
    echo "**************************************************************************************"
    echo ""

    echo "**************************************************************************************"
    echo "Removing wcms tests ..."
    rm -rf wcms_tests
    echo "Done removing wcms tests."
    echo "**************************************************************************************"
    echo ""

    echo "**************************************************************************************"
    echo "Clearing composer cache and running composer install ..."
    composer clearcache
    composer install --no-interaction
    if [ "$?" != 0 ]; then
      echo "Error: composer install failed (nogessoinstall)"
      exit 2;
    fi
    echo "Done clearing composer cache and running composer install."
    echo "**************************************************************************************"
    echo ""

    if [[ $wcms_option == 'y' ]]; then
      echo "**************************************************************************************"
      echo "Moving wcms config ..."
      if [[ -d ./web/config ]]; then
        rm -rf ./web/config
      fi
      mv config ./web/
      echo "Putting fonts in the right spot ..."
      cp -r /var/www/html/web/config/fonts /var/www/html/web
      echo "Done moving wcms config."
      echo "**************************************************************************************"
    fi

    # We don't need to recopy Ohana if the profile had the version they asked for.
    if [[ $default_ohana_version != $ohana_version ]]; then
      echo "**************************************************************************************"
      echo "Removing profile's ohana ..."
      rm -rf ./web/profiles/uw_base_profile/modules/custom/uw_wcms_ohana
      echo "Done removing profile's ohana."
      echo "**************************************************************************************"
      echo ""

      echo "**************************************************************************************"
      echo "Cloning ohana ${ohana_version}..."
      git clone --branch ${ohana_version} https://git.uwaterloo.ca/wcms/uw_wcms_ohana.git ./web/profiles/uw_base_profile/modules/custom/uw_wcms_ohana
      if [ "$?" != 0 ]; then
        echo "Error: git clone failed (noohanainstall)"
        exit 2;
      fi
      echo "Done cloning ohana ${ohana_version}."
      echo "**************************************************************************************"
      echo ""
    fi

    echo "**************************************************************************************"
    echo "Setting up ohana ..."
    ./web/profiles/uw_base_profile/modules/custom/uw_wcms_ohana/build_ohana.sh $env_option $build_option $PL_PATH $DRUPAL_PATH $os_option
    if [ "$?" != 0 ]; then
      echo "Error: build_ohana failed (noohanainstall)"
      exit 2;
    fi
    echo "Done setting up ohana."
    echo "**************************************************************************************"
    echo ""

    exit 0;

  elif [[ $build_option == 3 ]]; then

    if [[ -d web/profiles/uw_base_profile/modules/custom/uw_wcms_ohana ]]; then
      echo "**************************************************************************************"
      echo "Removing profile's ohana ..."
      rm -rf ./web/profiles/uw_base_profile/modules/custom/uw_wcms_ohana
      echo "Done removing profile's ohana."
      echo "**************************************************************************************"
      echo ""
    fi

    echo "**************************************************************************************"
    echo "Cloning ohana ${ohana_version}..."
    git clone --branch ${ohana_version} https://git.uwaterloo.ca/wcms/uw_wcms_ohana.git ./web/profiles/uw_base_profile/modules/custom/uw_wcms_ohana
    if [ "$?" != 0 ]; then
      echo "Error: git clone failed (onlyohanainstall)"
      exit 2;
    fi
    echo "Done cloning ohana ${ohana_version}."
    echo "**************************************************************************************"
    echo ""

    echo "**************************************************************************************"
    echo "Building ohana ..."
    ./web/profiles/uw_base_profile/modules/custom/uw_wcms_ohana/build_ohana.sh $env_option $build_option $PL_PATH $DRUPAL_PATH $os_option
    if [ "$?" != 0 ]; then
      echo "Error: build_ohana failed (onlyohanainstall)"
      exit 2;
    fi
    echo "Done building ohana."
    echo "**************************************************************************************"
    echo ""

    exit 0;

  elif [[ $build_option == 4 ]]; then

    if [ -f "$FILE" ]; then
      echo "**************************************************************************************"
      echo "Composer.lock exists, running composer update..."
      composer update
      if [ "$?" != 0 ]; then
        echo "Error: composer update failed (updatecomposerinstall)"
        exit 2;
      fi
      echo "Done running composer update."
      echo "**************************************************************************************"
      echo ""
    else
      echo "**************************************************************************************"
      echo "Composer.lock doesn't exist, running composer install..."
      composer install
      if [ "$?" != 0 ]; then
        echo "Error: composer install failed (updatecomposerinstall)"
        exit 2;
      fi
      echo "Done running composer install."
      echo "**************************************************************************************"
      echo ""
    fi

    exit 0;
  fi
}
