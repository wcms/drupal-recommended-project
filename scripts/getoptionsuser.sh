# Get the options for the script from user input.
function getoptionsuser {

  echo "";
  echo "You have chosen to rebuild or update the UW Base Profile and Ohana.";
  echo "";

  echo "What environment are you using?"
  echo "1 = Vagrant"
  echo "2 = D4d (docker based environments)"
  echo "3 = Lando"
  echo "4 = Pantheon"

  while [[ ! ${env_option} =~ ^(1|2|3|4)$ ]]; do
    read -rp "Environment option [2]: " env_option

    if [[ -z ${env_option} ]]; then
      env_option=2
    fi

    if [[ ! ${env_option} =~ ^(1|2|3|4)$ ]]; then
      echo "Invalid choice ..."
    fi
  done

  setenvoption

  if [[ ! ${env_option} =~ 4 ]]; then
    echo ""
    echo "What would you like to do?"
    echo "1 = $defaultinstall."
    echo "2 = $noohanainstall."
    echo "3 = $onlyohanainstall."
    echo "4 = $updatecomposerinstall."
    echo "[x] Exit and do nothing"

    while [[ ! ${build_option} =~ ^(1|2|3|4|x)$ ]]; do
      read -rp "Build option [1]: " build_option

      if [[ -z ${build_option} ]]; then
        build_option=1
      fi

      if [[ ! ${build_option} =~ ^(1|2|3|4|x)$ ]]; then
        echo "Invalid choice ..."
      fi
    done
  else
    build_option=2
  fi

  echo ""
  echo "What OS are you using?"
  echo "1 = mac"
  echo "2 = windows/linux."
  echo "[x] Exit and do nothing"
  while [[ ! ${os_option} =~ ^(1|2|x)$ ]]; do
    read -rp "OS option [1]: " os_option

    if [[ -z ${os_option} ]]; then
      os_option=1
    fi

    if [[ ! ${os_option} =~ ^(1|2|x)$ ]]; then
      echo "Invalid choice ..."
    fi
  done

  echo ""
  echo "Do you want to download WCMS config?"
  echo "y = Yes"
  echo "n = No"
  echo "[x] Exit and do nothing"
  while [[ ! ${wcms_option} =~ ^(y|n|x)$ ]]; do
    read -rp "WCMS config option [y]: " wcms_option

    if [[ -z ${wcms_option} ]]; then
      wcms_option='y'
    fi

    if [[ ! ${wcms_option} =~ ^(y|n|x)$ ]]; then
      echo "Invalid choice ..."
    fi
  done

  if [[ ! $build_option == $updatecomposerinstall ]]; then

    ohana_version=0

    while [[ ${ohana_version} == 0 ]]; do
      echo ""
      echo "What version of Ohana would you like to use?"
      read -rp "Ohana version [$default_ohana_version]: " ohana_version

      if [[ -z ${ohana_version} ]]; then
        ohana_version=$default_ohana_version
      fi

      local existed_in_remote=$(git ls-remote --quiet --heads --tags https://git.uwaterloo.ca/wcms/uw_wcms_ohana.git ${ohana_version} 2> /dev/null)

      if [[ -z ${existed_in_remote} ]]; then
        echo "Invalid branch ..."
        ohana_version=0
      fi
    done

  else
    ohana_version=$default_ohana_version
  fi
}
