# WCMS Recommended Project

** NOTE **
The instructiions below assume that the WCMS is using the Recommended Project
install. Otherwise, you will need to find the specific "recommended-project"
branches to use successfully.

The recommended project is the preferred way to install Drupal 8 using Composer.

All the WCMS related modules are now stored in the composer.json file and
running composer install, installs and enables them.

The uw_base_profile is now installed with the composer.json file as well and
there is no longer a rebuild.sh script in the profile folder.

## How to use

If you are already using WCMS Lando or the WCMS Vagrant 5.x branch, we install
Drupal using the recommended project already.

If not, clone this repo into a new directory (we recommend calling it
wcms-artifact).

```
git clone https://git.uwaterloo.ca/wcms/drupal-recommended-project wcms-artifact
```
Then, `cd` into the directory you cloned into and run composer install (the
example is for Lando).

```
cd /app/wcms-artifact && composer install
```
Nodejs will also need to be installed in your local environment. If it isn't
already, you can install it:

```
curl -sL https://deb.nodesource.com/setup_12.x | bash -
```
```
apt-get install -y nodejs
```
```
npm install -global gulp-cli
```
