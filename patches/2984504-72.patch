diff --git a/core/modules/taxonomy/src/Entity/Routing/VocabularyRouteProvider.php b/core/modules/taxonomy/src/Entity/Routing/VocabularyRouteProvider.php
index 71ea92e32c..9c144ef4e7 100644
--- a/core/modules/taxonomy/src/Entity/Routing/VocabularyRouteProvider.php
+++ b/core/modules/taxonomy/src/Entity/Routing/VocabularyRouteProvider.php
@@ -49,7 +49,7 @@ protected function getResetPageRoute(EntityTypeInterface $entity_type) {
     $route = new Route('/admin/structure/taxonomy/manage/{taxonomy_vocabulary}/reset');
     $route->setDefault('_entity_form', 'taxonomy_vocabulary.reset');
     $route->setDefault('_title', 'Reset');
-    $route->setRequirement('_permission', $entity_type->getAdminPermission());
+    $route->setRequirement('_entity_access', 'taxonomy_vocabulary.reset all weights');
     $route->setOption('_admin_route', TRUE);
     $route->setOption('parameters', [
       'taxonomy_vocabulary' => [
diff --git a/core/modules/taxonomy/src/Form/OverviewTerms.php b/core/modules/taxonomy/src/Form/OverviewTerms.php
index ebc9e5933c..0b55e8a945 100644
--- a/core/modules/taxonomy/src/Form/OverviewTerms.php
+++ b/core/modules/taxonomy/src/Form/OverviewTerms.php
@@ -3,7 +3,6 @@
 namespace Drupal\taxonomy\Form;
 
 use Drupal\Component\Utility\Unicode;
-use Drupal\Core\Access\AccessResult;
 use Drupal\Core\Entity\EntityRepositoryInterface;
 use Drupal\Core\Entity\EntityTypeManagerInterface;
 use Drupal\Core\Form\FormBase;
@@ -300,13 +299,14 @@ public function buildForm(array $form, FormStateInterface $form_state, Vocabular
 
     // Only allow access to change parents and reorder the tree if there are no
     // pending revisions and there are no terms with multiple parents.
-    $update_tree_access = AccessResult::allowedIf(empty($pending_term_ids) && $vocabulary_hierarchy !== VocabularyInterface::HIERARCHY_MULTIPLE);
-
+    $update_tree_access = $taxonomy_vocabulary->access('reset all weights', NULL, TRUE);
     $form['help'] = [
       '#type' => 'container',
       'message' => ['#markup' => $help_message],
     ];
-    if (!$update_tree_access->isAllowed()) {
+
+    $operations_access = !empty($pending_term_ids) || $vocabulary_hierarchy === VocabularyInterface::HIERARCHY_MULTIPLE;
+    if ($operations_access) {
       $form['help']['#attributes']['class'] = ['messages', 'messages--warning'];
     }
 
@@ -327,7 +327,7 @@ public function buildForm(array $form, FormStateInterface $form_state, Vocabular
       '#header' => [
         'term' => $this->t('Name'),
         'operations' => $this->t('Operations'),
-        'weight' => $update_tree_access->isAllowed() ? $this->t('Weight') : NULL,
+        'weight' => !$operations_access ? $this->t('Weight') : NULL,
       ],
       '#attributes' => [
         'id' => 'taxonomy',
@@ -394,8 +394,6 @@ public function buildForm(array $form, FormStateInterface $form_state, Vocabular
           ],
         ];
       }
-      $update_access = $term->access('update', NULL, TRUE);
-      $update_tree_access = $update_tree_access->andIf($update_access);
 
       if ($update_tree_access->isAllowed()) {
         $form['terms'][$key]['weight'] = [
diff --git a/core/modules/taxonomy/src/VocabularyAccessControlHandler.php b/core/modules/taxonomy/src/VocabularyAccessControlHandler.php
index 176691408a..783850758c 100644
--- a/core/modules/taxonomy/src/VocabularyAccessControlHandler.php
+++ b/core/modules/taxonomy/src/VocabularyAccessControlHandler.php
@@ -23,6 +23,13 @@ protected function checkAccess(EntityInterface $entity, $operation, AccountInter
       case 'view':
         return AccessResult::allowedIfHasPermissions($account, ['access taxonomy overview', 'administer taxonomy'], 'OR');
 
+      case 'reset all weights':
+        return AccessResult::allowedIfHasPermission($account, 'administer taxonomy')
+          ->orIf(AccessResult::allowedIfHasPermissions($account, [
+            'access taxonomy overview',
+            'edit terms in ' . $entity->id(),
+          ]));
+
       default:
         return parent::checkAccess($entity, $operation, $account);
     }
diff --git a/core/modules/taxonomy/tests/src/Functional/VocabularyPermissionsTest.php b/core/modules/taxonomy/tests/src/Functional/VocabularyPermissionsTest.php
index 1794ecde34..990815ca37 100644
--- a/core/modules/taxonomy/tests/src/Functional/VocabularyPermissionsTest.php
+++ b/core/modules/taxonomy/tests/src/Functional/VocabularyPermissionsTest.php
@@ -92,6 +92,9 @@ public function testTaxonomyVocabularyOverviewPermissions() {
     $assert_session->fieldExists('Weight');
     $assert_session->pageTextContains($edit_help_text);
 
+    $this->submitForm([], 'Reset to alphabetical');
+    $assert_session->statusCodeEquals(200);
+
     // Visit vocabulary overview without terms. 'Add term' should be shown.
     $this->drupalGet('admin/structure/taxonomy/manage/' . $vocabulary2_id . '/overview');
     $assert_session->statusCodeEquals(200);
@@ -115,6 +118,7 @@ public function testTaxonomyVocabularyOverviewPermissions() {
     $assert_session->linkNotExists('Edit');
     $assert_session->linkNotExists('Delete');
     $assert_session->buttonNotExists('Save');
+    $assert_session->buttonNotExists('Reset to alphabetical');
     $assert_session->pageTextContains('Weight');
     $assert_session->fieldNotExists('Weight');
     $assert_session->linkNotExists('Add term');
@@ -144,6 +148,9 @@ public function testTaxonomyVocabularyOverviewPermissions() {
     $assert_session->linkNotExists('Add term');
     $assert_session->pageTextContains($edit_help_text);
 
+    $this->submitForm([], 'Reset to alphabetical');
+    $assert_session->statusCodeEquals(200);
+
     // Visit vocabulary overview without terms. 'Add term' should not be shown.
     $this->drupalGet('admin/structure/taxonomy/manage/' . $vocabulary2_id . '/overview');
     $assert_session->statusCodeEquals(200);
@@ -163,6 +170,7 @@ public function testTaxonomyVocabularyOverviewPermissions() {
     $assert_session->linkExists('Delete');
     $assert_session->linkNotExists('Add term');
     $assert_session->buttonNotExists('Save');
+    $assert_session->buttonNotExists('Reset to alphabetical');
     $assert_session->pageTextContains('Weight');
     $assert_session->fieldNotExists('Weight');
     $assert_session->pageTextContains($no_edit_help_text);
@@ -191,6 +199,8 @@ public function testTaxonomyVocabularyOverviewPermissions() {
     $assert_session->pageTextContains('Weight');
     $assert_session->fieldExists('Weight');
     $assert_session->pageTextContains($edit_help_text);
+    $this->submitForm([], 'Reset to alphabetical');
+    $assert_session->statusCodeEquals(200);
 
     // Visit vocabulary overview without terms. 'Add term' should not be shown.
     $this->drupalGet('admin/structure/taxonomy/manage/' . $vocabulary2_id . '/overview');
@@ -212,6 +222,7 @@ public function testTaxonomyVocabularyOverviewPermissions() {
     $assert_session->linkNotExists('Delete');
     $assert_session->linkExists('Add term');
     $assert_session->buttonNotExists('Save');
+    $assert_session->buttonNotExists('Reset to alphabetical');
     $assert_session->pageTextContains('Weight');
     $assert_session->fieldNotExists('Weight');
     $assert_session->pageTextContains($no_edit_help_text);
